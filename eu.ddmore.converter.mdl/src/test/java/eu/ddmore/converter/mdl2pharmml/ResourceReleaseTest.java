package eu.ddmore.converter.mdl2pharmml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import eu.ddmore.convertertoolbox.api.response.ConversionDetail;
import eu.ddmore.convertertoolbox.api.response.ConversionDetail.Severity;
import eu.ddmore.convertertoolbox.api.response.ConversionReport;
import eu.ddmore.convertertoolbox.api.response.ConversionReport.ConversionCode;


public class ResourceReleaseTest {

    @Rule
    public TemporaryFolder workingFolder = new TemporaryFolder();

    private File validMdlFile;
    private File validPharmmlFile;
    private File dataFile;
    
    private MDLToPharmMLConverter converter;

    @Before
    public void setUp() throws IOException {
    
        validMdlFile = new File(workingFolder.getRoot(), "valid.mdl");
        validPharmmlFile = new File(workingFolder.getRoot(), "valid.xml");
        
        FileUtils.copyURLToFile(getClass().getResource("/valid.mdl"), validMdlFile);
        
        dataFile = new File(workingFolder.getRoot(), "warfarin_conc.csv");
        FileUtils.copyURLToFile(getClass().getResource("/warfarin_conc.csv"), dataFile);
        
        this.converter = new MDLToPharmMLConverter();
    }
    
    @Test
    public void testPerformConvertForValidMdlFile() throws IOException {
    		{
	        assertFalse("Converted PharmML file should not initially exist", validPharmmlFile.exists());
	        final ConversionReport report = converter.performConvert(validMdlFile, workingFolder.getRoot());
	        assertEquals("Checking for successful return code", ConversionCode.SUCCESS, report.getReturnCode());
	        assertTrue("Converted PharmML file should have been created", validPharmmlFile.exists());
	        final List<ConversionDetail> errors = report.getDetails(Severity.ERROR);
	        assertTrue("Checking that no errors were returned", errors.isEmpty());
	        final List<ConversionDetail> warnings = report.getDetails(Severity.WARNING);
	        assertTrue("Checking that no warnings were returned", warnings.isEmpty());
    		}
        validPharmmlFile.delete();
        {
	        assertFalse("Converted PharmML file should not initially exist", validPharmmlFile.exists());
	        try(OutputStream os = new FileOutputStream(validMdlFile)){
	        		URL fileUrl = getClass().getResource("/syntaxerrors.mdl");
		        FileUtils.copyFile(new File(fileUrl.getFile()), os);
	        }
	        final ConversionReport report = converter.performConvert(validMdlFile, workingFolder.getRoot());
	        assertEquals("Checking for successful return code", ConversionCode.FAILURE, report.getReturnCode());
	        assertFalse("Converted PharmML file should have been created", validPharmmlFile.exists());
        }
    }


}
