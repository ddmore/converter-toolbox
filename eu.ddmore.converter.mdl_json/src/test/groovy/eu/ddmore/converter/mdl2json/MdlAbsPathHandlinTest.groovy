/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, http://www.mango-solutions.com
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 *******************************************************************************/
package eu.ddmore.converter.mdl2json

import static org.junit.Assert.*

import java.nio.file.Path
import java.util.stream.Stream
import java.nio.file.Files

import static eu.ddmore.converter.mdl2json.MdlAndJsonFileUtils.*
import static eu.ddmore.converter.mdl2json.testutils.MdlFileContentTestUtils.*
import eu.ddmore.converter.mdl2json.domain.Mcl
import eu.ddmore.converter.mdl2json.utils.Mdl2JsonExpressionConverter

import org.junit.rules.TemporaryFolder;

import org.apache.commons.io.FileUtils
import org.apache.commons.lang.StringUtils
import org.apache.log4j.Logger
import org.junit.Ignore;
import org.junit.Rule
import org.junit.Test

class MdlAbsPathHandlinTest {
    private static final Logger LOGGER = Logger.getLogger(MdlAbsPathHandlinTest.class)
    
	@Rule
	public TemporaryFolder tmpDir = new TemporaryFolder()
	
	@Test
    public void testAbsPathHandlingForMDL2Json() {
		final def regex =  /file\s*=\s*"([^"]+)"/	
		def dest = tmpDir.newFile("absPath.mdl");
		String absPath =  tmpDir.getRoot().getAbsolutePath();
		def replaceText = "file = \"" + absPath + "/\$1\"";
        def origMdlFile = getFile("Magni_2000_diabetes_C-peptide.mdl")
		dest.withWriter { w->
			origMdlFile.eachLine{ line ->
				w << line.replaceFirst(regex, replaceText) + System.getProperty("line.separator")
			}
		}
		def outFile = Files.createTempFile("Magni_2000_diabetes_C-peptide", "json")
        def converter = new MDLToJSONConverter()
		converter.performConvertToFile(dest, outFile.toFile())
        assertTrue(Files.exists(outFile.toAbsolutePath()))
		def actualFileContent = []
		final def jsonRegex =  /"file"\s*:\s*"\\"([^"]+)\\"/
		outFile.toFile().readLines().find({ line ->
			 def matcher = line =~ jsonRegex
			 if(matcher) {
				 actualFileContent.add(matcher[0][1])
			 }
			})
		assertFalse(actualFileContent.isEmpty())
		assertTrue(new File(actualFileContent.get(0)).absolute)
    }
    
	@Test
    public void testAbsPathHandlingForJson2Mdl() {
		final def jsonRegex =  /"file"\s*:\s*"\\"([^"]+)\\"/ 
		def dest = tmpDir.newFile("absPath.json");
		String absPath =  tmpDir.getRoot().getAbsolutePath();
		def replaceText = "\"file\" : \"\\\\\"" + absPath + "/\$1\\\\\"";
		System.out.println(replaceText)
        def origMdlFile = getFile("Magni_2000_diabetes_C-peptide.json")
		dest.withWriter { w->
			origMdlFile.eachLine{ line ->
				w << line.replaceFirst(jsonRegex, replaceText) + System.getProperty("line.separator")
			}
		}
		def outFile = Files.createTempFile("Magni_2000_diabetes_C-peptide", ".mdl")
        def converter = new JSONToMDLConverter()
		converter.performConvertToFile(dest, outFile.toFile())
        assertTrue(Files.exists(outFile.toAbsolutePath()))
		def actualFileContent = []
		final def mdlRegex =  /file\s*=\s*"([^"]+)"/	
		outFile.toFile().readLines().find({ line ->
			 def matcher = line =~ mdlRegex
			 if(matcher) {
				 actualFileContent.add(matcher[0][1])
			 }
			})
		assertFalse(actualFileContent.isEmpty())
		assertTrue(new File(actualFileContent.get(0)).absolute)
    }
    
}
