/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, http://www.mango-solutions.com
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 *******************************************************************************/
package eu.ddmore.converter.mdl2json

import static org.junit.Assert.*

import java.nio.file.Path
import java.nio.file.Files

import static eu.ddmore.converter.mdl2json.MdlAndJsonFileUtils.*
import static eu.ddmore.converter.mdl2json.testutils.MdlFileContentTestUtils.*
import eu.ddmore.converter.mdl2json.domain.Mcl
import org.junit.rules.TemporaryFolder;

import org.apache.commons.io.FileUtils
import org.apache.commons.lang.StringUtils
import org.apache.log4j.Logger
import org.junit.Ignore;
import org.junit.Rule
import org.junit.Test

class JsonGenerationTest {
    private static final Logger LOGGER = Logger.getLogger(JsonGenerationTest.class)
    
	@Rule
	public TemporaryFolder tmpDir = new TemporaryFolder()
	
//	@Test
//    public void testImplicitVectorHandlineMDLToJson() {
//		final def regex =  /"file"\s*:\s*"(.+)"\s*,/
//        def origMdlFile = getFile("UseCase1_Product5.mdl")
//		def expectedMdlFile = []
////		getFile("UseCase1_Product5.json").eachLine({	line ->
////			 def matcher = line =~ regex
////			 if(matcher) {
////				 expectedMdlFile.add(matcher[0][1])
////			 }
////			})
//		
//		def outFile = Files.createTempFile("UseCase1_Product5", ".json")
//        def converter = new MDLToJSONConverter()
//		converter.performConvertToFile(origMdlFile, outFile.toFile())
//        assertTrue(Files.exists(outFile.toAbsolutePath()))
//		def actualFileContent = []
//		outFile.toFile().readLines().find({ line ->
//			 def matcher = line =~ regex
//			 if(matcher) {
//				 actualFileContent.add(matcher[0][1])
//			 }
//			})
//		assertFalse(actualFileContent.isEmpty())
//		assertEquals(expectedMdlFile, actualFileContent)
//    }
    
	@Test
    public void testEscapeCharacterHandlingOnMDLToJson() {
		final def regex =  /"file"\s*:\s*"(.+)"\s*,/
        def origMdlFile = getFile("Magni_2000_clean_MVprova1.mdl")
		def expectedMdlFile = []
		getFile("Magni_2000_clean_MVprova1.json").eachLine({	line ->
			 def matcher = line =~ regex
			 if(matcher) {
				 expectedMdlFile.add(matcher[0][1])
			 }
			})
		
		def outFile = Files.createTempFile("Magni_2000_clean_MVprova1", "json")
        def converter = new MDLToJSONConverter()
		converter.performConvertToFile(origMdlFile, outFile.toFile())
        assertTrue(Files.exists(outFile.toAbsolutePath()))
		def actualFileContent = []
		outFile.toFile().readLines().find({ line ->
			 def matcher = line =~ regex
			 if(matcher) {
				 actualFileContent.add(matcher[0][1])
			 }
			})
		assertFalse(actualFileContent.isEmpty())
		assertEquals(expectedMdlFile, actualFileContent)
    }
    
 }
