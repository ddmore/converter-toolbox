package eu.ddmore.converter.mdl2json.utils

import eu.ddmore.mdl.mdl.FuncArguments
import eu.ddmore.mdl.mdl.StringLiteral
import eu.ddmore.mdl.utils.MdlExpressionConverter
import eu.ddmore.mdllib.mdllib.Expression
import org.apache.commons.lang.StringEscapeUtils

public class Mdl2JsonExpressionConverter extends MdlExpressionConverter {
	
    private final static Mdl2JsonExpressionConverter INSTANCE = new Mdl2JsonExpressionConverter()
    
    public def static String convertToString(Expression expr){
        INSTANCE.getString(expr)
    }
    
    public def static String convertToString(FuncArguments funcArgs) {
        INSTANCE.getString(funcArgs)
    }
    
    /**
     * Override the method from the superclass so that strings are enclosed in double quotes when writing out MDL.
     */
    override dispatch String getString(StringLiteral exp)'''
	"«StringEscapeUtils.escapeJava(exp.value)»"'''

}
